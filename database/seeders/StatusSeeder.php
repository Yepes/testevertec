<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = new Status();
        $status->status_description = "CREATED";
        $status->save();
        
        $status1 = new Status();
        $status1->status_description = "REJECTED";
        $status1->save();

        $status2 = new Status();
        $status2->status_description = "PAYED";
        $status2->save();

        $status2 = new Status();
        $status2->status_description = "PENDING";
        $status2->save();
    }
}
