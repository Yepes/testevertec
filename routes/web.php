<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Order\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', HomeController::class)->name('home');



Route::resource('order', OrderController::class);
Route::get('preview/{order}', [OrderController::class, 'preview'])->name('order.preview');
Route::post('postPreview', [OrderController::class, 'postPreview'])->name('order.postPreview');
Route::get('viewState/{id}', [OrderController::class, 'viewState'])->name('order.viewState');
Route::get('retryPay/{id}', [OrderController::class, 'retryPay'])->name('order.retryPay');




