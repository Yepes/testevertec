@extends('layouts.main')

@section('title', 'Home')

@section('content')
    <h1> Amazing TShirt</h1><br>
    <img src="{{asset('img/tshirt.jpg')}}" alt=""><br>
    <span class="title">$15</span><br>
    <a class="btn btn-success" href="{{route('order.create')}}">Buy now</a>
    
@endsection
