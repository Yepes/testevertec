@extends('layouts.main')

@section('title', 'Index')

@section('content')
    <h1>My order #{{$order->id}}</h1>
    <table>
        <tr>
            <th>#</th>
            <th>Customer</th>
            <th>Status</th>
        </tr>
        <tr>
            <td>{{$order->id}}</td>
            <td>{{$order->customer->customer_name}}</td>
            <td>
                @if($order->status_id != '3')
                    <a class="btn btn-warning" href="{{route('order.viewState',['id'=>$order->id])}}"> {{ $order->status->status_description }}</a>
                @else
                    {{ $order->status->status_description }}
                @endif
            </td>
        </tr>
    </table>

@endsection