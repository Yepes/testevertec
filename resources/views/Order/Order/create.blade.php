@extends('layouts.main')

@section('title', 'Confirm Purchase')

@section('content')
    <h1>Confirm Purchase</h1><br>
    <form action="{{route('order.store')}}" method="POST">

        <label>
            <span class="title">In Cart</span> <br><img src="{{asset('img/tshirt.jpg')}}" alt=""><br><span class="title">$15</span>
        </label><br><br>

        @csrf
        <label>
            Name
            <br>
            <input type="text" name="customer_name" value="{{old('customer_name')}}">
        </label>
        @error('customer_name')
            <br>
            <small>*{{$message}}</small>
            <br>
        @enderror
        <br>
        <label>
            Email
            <br>
            <input type="email" name="customer_email" value="{{old('customer_email')}}">
        </label>
        @error('customer_email')
            <br>
            <small>*{{$message}}</small>
            <br>
        @enderror
        <br>
        <label>
            Mobile Phone
            <br>
            <input type="text" name="customer_mobile" value="{{old('customer_mobile')}}">
        </label>
        @error('customer_mobile')
            <br>
            <small>*{{$message}}</small>
            <br>
        @enderror
        <br>
        <button class="btn btn-success" type="submit">Continue to checkout</button>
    </form>
@endsection
