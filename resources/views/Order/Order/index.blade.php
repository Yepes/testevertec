@extends('layouts.main')

@section('title', 'Index')

@section('content')
    <h1>Orders from the Store</h1>
    <table class="table table-striped">
        <tr>
            <th>#</th>
            <th>Customer</th>
            <th>Status</th>
        </tr>
        @foreach ($orders as $order)
            <tr>
                <td>{{$order->id}}</td>
                <td>{{$order->customer->customer_name}}</td>
                <td>
                    @if($order->status_id != '3')
                        <a class="btn btn-warning" href="{{route('order.viewState',['id'=>$order->id])}}"> {{ $order->status->status_description }}</a>
                    @else
                        {{ $order->status->status_description }}
                    @endif
                </td>
            </tr>
        @endforeach
    </table>

    {{$orders->links()}}
@endsection
