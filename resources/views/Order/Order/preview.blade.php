@extends('layouts.main')
@section('title', 'Order Preview')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p>Se han produccido los siguientes errores:</p>
            <ul>
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>Resumen de la compra</h1>
        <img src="{{asset('img/tshirt.jpg')}}" alt="">
            <div>
                <h3>DualShock Controller for PlayStation 4</h3>
                <div class="mb-2">
                    $15
                </div>
                <div class="clearfix"></div>
            </div>
    <div class="container">
        <div style="display: flex;  align-items: center;  justify-content: center;">
            <form method="POST" action="{{ route('order.postPreview') }}">
                @csrf

                <legend>Necesary data</legend>
                <div class="form-group">
                    <label for="name" class="form-label">Nombre</label>
                    <input type="text" id="name" name="name" class="form-control" placeholder="Ingrese su nombre"
                           maxlength="80" value="{{ old('customer_name',$customer->customer_name) }}" required readonly>
                </div>
                <div class="form-group">
                    <label for="email" class="form-label">Email *</label>
                    <input type="email" id="email" name="email" class="form-control"
                           placeholder="Ingrese su correo electr&oacute;nico" maxlength="120" value="{{ old('customer_email',$customer->customer_email) }}" required readonly>
                </div>
                <div class="form-group">
                    <label for="mobile" class="form-label">Celular *</label>
                    <input type="number" id="mobile" name="mobile" class="form-control"
                           placeholder="Ingrese su numero de celular" maxlength="40" value="{{ old('customer_mobile',$customer->customer_mobile) }}" required readonly>
                </div>
                <div>
                    <input type="hidden" name="id_orden" value="{{$order->id}}">
                    <a href="{{route('order.create', ['id' => $order->id])}}" type="submit" class="btn btn-primary m-5">Volver</a>
                    <button type="submit" class="btn btn-primary m-5">Comprar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
