@extends('layouts.main')
@section('title', 'Status order')
@section('content')
    <label>
        <span class="title">In Cart</span> <br><img src="{{asset('img/tshirt.jpg')}}" alt=""><br><span class="title">$15</span>
    </label><br><br>
    
    <div class=" align-items-center">
        <div class="col-12 text-center">
            <h2>Estado del producto</h2>
        </div>
        <div class="col-12 text-center">
            <label> Estado del producto: <strong>{{$statusTra[$response['status']['status']]}}</strong> </label>
        </div>
        <div class="col-12 text-center">
            <label> Detalles: {{$response['status']['message']}} </label>
        </div>
        @if($response['status']['status'] == 'REJECTED')
            <div class="col-12 text-center p-2">
                <a class="btn btn-danger" href="{{route("retryPayOrder", ['id' => $order->id])}}" role="button">Reintentar</a>
            </div>
        @endif
        @if($response['status']['status'] == 'PENDING')
            <div class="col-12 text-center p-2">
                <a class="btn btn-warning" href="{{$order->process_url}}" role="button">Reintentar</a>
            </div>
        @endif
        <div class="col-12 text-center p-2">
            <a href="{{route('order.show',$order->id)}}" class="btn btn-primary" >Listar ordenes</a>
        </div>
    </div>
@endsection
