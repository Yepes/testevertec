@extends('layouts.main')

@section('title', 'Edit')

@section('content')
    <h1>create orden</h1>
    <form action="{{route('order.update', $order)}}" method="POST">
        @csrf

        @method('put')
        <label>
            Nombre
            <br>
            <input type="text" name="customer_name" value="{{ old('customer_name',$order->customer_name) }}">
        </label>
        @error('customer_name')
            <br>
            <small>*{{$message}}</small>
            <br>
        @enderror
        <br>
        <label>
            Correo
            <br>
            <input type="email" name="customer_email" value="{{ old('customer_email',$order->customer_email) }}">
        </label>
        @error('customer_email')
            <br>
            <small>*{{$message}}</small>
            <br>
        @enderror
        <br>
        <label>
            Celular
            <br>
            <input type="text" name="customer_mobile" value="{{ old('customer_mobile',$order->customer_mobile) }}">
        </label>
        @error('customer_mobile')
            <br>
            <small>*{{$message}}</small>
            <br>
        @enderror
        <br>
        <button type="submit">Actualizar</button>
    </form>
@endsection
