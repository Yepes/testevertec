<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.partials.head')
    <!--Favicon-->
    <!--Estilos-->
</head>
<body>
    <!--Header-->
    <!--Nav-->
    @include('layouts.partials.header')
    <div class="content">
        @yield('content')
    </div>
    <!--Footer-->
    @include('layouts.partials.footer')
    <!--Script-->
</body>
</html>