<header>
    <h1>Tienda Online</h1>
    <nav>
        <ul>
            <li><a href="{{route('home')}}" class="{{ request()->routeIs('home') ? 'active' : '' }}">Home</a></li>
            <li><a href="">My orders</a></li>
            <li><a href="{{route('order.index')}}" class="{{ request()->routeIs('order.index') ? 'active' : '' }}">Store orders</a></li>
        </ul>
    </nav>
</header>