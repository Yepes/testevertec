<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Utils\Helpers;

use App\Http\Requests\StoreOrder;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->utils = new Helpers();
        $this->status = [
            'CREATED' => 1,
            'REJECTED' => 2,
            'APPROVED' => 3,
            'PENDING' => 4
        ];
    }
    public function index(){
        $orders = Order::orderBy('id','desc')->paginate(5);
        return view('Order.Order.index', compact('orders'));
    }

    public function create(){
        return view('Order.Order.create');
    }

    public function store(StoreOrder $request){

        $customer = Customer::create($request->all());
        
        $order = new Order();

        $order->status_id = 1;
        $order->customer_id = $customer->id;
        $order->product_id = 1;

        $order->save();

        return redirect()->route('order.preview', $order->id);
    }

    public function show(Order $order){
        return view('Order.Order.show', compact('order'));
    }

    public function edit(Customer $order){
        return view('Order.Order.edit', compact('order'));
    }

    public function update(StoreOrder $request, Customer $order){

        $order->update($request->all());

        return redirect()->route('order.show', $order->id);
    }

    public static function updateOrder($id, $status, $id_request = false, $process_url = false)
    {
        $orders = Order::find($id);
        $orders->status_id = $status;
        if($id_request){
            $orders->id_request = $id_request;
            $orders->process_url = $process_url;
        }
        if (!$orders->save()) {
            return false;
        }
        return $orders;
    }

    public function preview($id)
    {
        $order = Order::find($id);
        if($order == null || empty($order) ){
            return redirect()->route('order.create')->withErrors(['errors' => 'El id especificado no existe']);
        }
        $customer = Customer::find($order->customer_id);
        return view('Order.Order.preview', compact('order','customer'));
    }

    public function postPreview(Request $request)
    {

        $order = Order::find($request['id_orden']);
        if($order == null || empty($order) ){
            return redirect()->route('order.create');
        }
        $responseWs = $this->utils->createSessionPlacetoPay($order->id);
        
        if ($responseWs['status']['status'] === 'OK' && $responseWs['status']['reason'] === 'PC') {
            $this->updateOrder($order->id, 1, $responseWs['requestId'], $responseWs['processUrl']);
            return redirect($responseWs['processUrl']);
        } else {
            $customer = Customer::find($order->customer_id);
            return redirect()->route('order.preview',$order->id,$customer)->withErrors(['errors' => 'Ocurrio un error al consumir el servicio, por favor intentar mas tarde']);
        }
    }

    public function viewState($id)
    {
        $statusTra = [
            'CREATED' => 'Creada',
            'REJECTED' => 'Rechazada',
            'APPROVED' => 'Pagada',
            'PENDING' => 'Pendiente'
        ];
        $order = Order::find($id);
        if($order == null || empty($order) ){
            return redirect()->route('order.show');
        }
        $customer = Customer::find($order->customer_id);
        if($order->id_request !== null){
            $response = $this->utils->getRequestInformation($order->id_request);
            $this->updateOrder($order->id, $this->status[$response['status']['status']]);
            return view('Order.Order.statusOrder', compact('response', 'statusTra', 'order'));
        }else{

            return view('Order.Order.preview', compact('order','customer'));
        }
    }
}
