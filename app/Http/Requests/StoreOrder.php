<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'customer_name' => 'required',
            'customer_email' => 'required|email',
            'customer_mobile' => 'required'
        ];
    }

    public function attributes(){
        return [
            'customer_name' => 'nombre',
            'customer_email' => 'email',
            'customer_mobile' => 'celular'
        ];
    }

    public function messages(){
        return [
            'customer_email.email' => 'El campo email debe contener una direcci&oacute;n de correo valido',
        ];
    }
}
